## React Electron Starter Files

In the project directory, you can run:

### `yarn run dev or npm dev`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The desktop application will reload if you make edits.<br />
You will also see any lint errors in the console.
