const electron = require('electron');
//Module to control application life
const app = electron.app;
//Module to create native browser window
const BrowserWindow = electron.BrowserWindow;

const path = require('path');
const url = require('url');
require('electron-reload')

//keep the global reference of the window object, if you dont, the window will be closed automatically when the javascript object is garbage collected.
let mainWindow;

const createWindow = () => {
  //create the browser window
  mainWindow = new BrowserWindow({
    width:800,
    height:600,
    webPreferences:{
      nodeIntegration:true
    }
  });

  //and load the index.html of the application
  const startUrl = process.env.ELECTRON_START_URL || url.format({
    pathname:path.join(__dirname, '/../build/index.html'),
    protocol:'file',
    slashes:true
  });

  mainWindow.loadURL(startUrl);
  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  mainWindow.on('closed', () => {
    //Dereference the window object, usually you would store windows
    //in an array if your app supports multi-windows, this is the time
    //when you should delete the corresponding element
    mainWindow = null;
  });
};

//This method will be called when electron has finished
//initialisation and is ready to create browser windows
//Some apis can only be used after this event occurs
app.on('ready', createWindow);

//quit when all windows are closed
app.on('window-all-closed', () => {
  //On OS x, it is common for applications and their menu bar
  //to stay active until  user quits explicitly with Cmd + Q
  if(process.platform !== 'darwin')
    app.quit();
});

app.on('activate', () => {
  //On Osx its common to re-create a window in the app when the dock icon is clicked and  there are no other windows open
  if(mainWindow === null)
    createWindow();
});

// require app specific main process code here
