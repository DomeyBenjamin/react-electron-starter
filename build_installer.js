// build installer

//1. Import modules
const { MSICreator } = require('electron-wix-msi');
const path = require('path');

//2. Define the input and output directory.
//Important: the directories must be obsolute and not relative
const APP_DIR = path.resolve(__dirname, './dist/win-unpacked');
const OUT_DIR = path.resolve(__dirname, './windows_installer');

const msiCreator = new MSICreator({
  appDirectory:APP_DIR,
  outputDirectory:OUT_DIR,

  //configure the metadata
  description:"Church Management System",
  exe:"PICC Church",
  name:"PICC Church",
  manufacturer:'Eben Nyarko',
  version:'1.0.0',

  //configure installer User Interface
  ui:{
    chooseDirectory:true
  }
});

//4. create a .wxs template file
msiCreator.create().then(()=>{

  //5. compile the template to a .msi file
  msiCreator.compile();
});
